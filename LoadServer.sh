#!/bin/bash

## Autor: Gustavo Nogueira ##
## Função:  Verificação de uso de recursos do servidor: ##
## Memória RAM, SWAP. Gerar relatórios dos mesmos e ##
## enviar para email de cliente ##


# Verifica se o pacote está instalado  e caso não esteja, será instalado #
PACOTE=$(rpm -qa | grep sysstat)
SYS=$(sysstat)

clear
echo ""
sleep 2
echo "Verificando se o pacote sysstat está instalado no servidor..."
sleep 3

if [ $PACOTE ]
    then
        echo "O pacote $SYS está instalado"
        echo ""
    else
        echo "O pacote $SYS será instalado..."
        echo ""
sleep 4
        yum install sysstat -y
sleep 4
        echo "O apcote foi instalado com sucesso"
sleep 3
    elif
        echo "Verificando a quantidade de memória "
        sar -r | awk '{print $4}'
fi

